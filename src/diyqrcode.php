<?php

/**
 * 类名：DiyQrcode
 * 作者：mqycn
 * 博客：http://www.miaoqiyuan.cn
 * 源码：http://gitee.com/mqycn/diy-qrcode/
 * 说明：核心类
 */

// 依赖：https://sourceforge.net/projects/phpqrcode/
require_once dirname(__FILE__) . '/../vendor/phpqrcode.php';

class DiyQrcode {

	private $qrLevel = 'M'; //二维码校正级别，可选：L、M、Q、H
	private $qrMatrix = 6; //矩阵的大小， 1-10

	private $qrTemplate = 'http://gitee.com/mqycn/diy-qrcode?qrcode=[KEY]'; //落地页路径模版

	private $qrType = 'png'; //二维码 输出类型

	private $qrX = 0; //插入点 X 的位置
	private $qrY = 0; //插入点 y 的位置
	private $qrW = 100; //二维码宽度
	private $qrH = 100; //二维码高度

	private $qrBackground = 'background.jpg'; //海报背景图
	private $qrSkin = 'skin1'; //海报背景图存放路径，替换路径中的[SKIN]

	private $key = 'DiyQrcode Demo'; //二维码正文内容
	private $cachePath = './cache/'; //临时文件保存路径

	public function __construct($config = array()) {
		$this->setConf($config);
	}

	/**
	 * 设置配置信息
	 */
	public function setConf($config = array()) {
		if (!is_array($config)) {
			$config = array();
		}

		//设置校正级别
		$this->saveConfig($config, 'level', array('L', 'M', 'Q', 'H'));

		//设置矩阵大小
		$this->saveConfig($config, 'matrix', '/^[0-9]$/');

		//设置矩阵大小
		$this->saveConfig($config, 'type', array('png', 'jpg'));

		//设置落地页地址
		$this->saveConfig($config, 'template');

		//设置背景图片地址和路径
		$this->saveConfig($config, 'skin');
		$this->saveConfig($config, 'background');

		//设置二维码的位置和尺寸
		$this->saveConfig($config, 'x', '/^[0-9]{1,4}$/');
		$this->saveConfig($config, 'y', '/^[0-9]{1,4}$/');
		$this->saveConfig($config, 'w', '/^[0-9]{1,4}$/');
		$this->saveConfig($config, 'h', '/^[0-9]{1,4}$/');

		list($image_path, $image_ext) = $this->getBackground();
		if (!is_file($image_path)) {
			throw new Exception("背景图片({$image_path})不存在", 1);
			die();
		}
	}

	/**
	 * 设置二维码正文
	 */
	public function setKey($key = '') {
		$this->key = $key;
	}

	/**
	 * 输出二维码
	 */
	public function output($file_name = null) {

		//打开背景图
		$imgbg = $this->openBackground();

		//生成二维码
		$qrimg = $this->createQrcode();
		$qrcode = imagecreatefrompng($qrimg);
		list($src_width, $src_height) = getimagesize($qrimg);

		//合并图片
		$dst_x = $this->qrX; //目标图像开始 x 坐标
		$dst_y = $this->qrY; //目标图像开始 y 坐标，x,y同为 0 则从左上角开始

		$src_x = 0; //拷贝图像开始 x 坐标
		$src_y = 0; //拷贝图像开始 y 坐标，x,y同为 0 则从左上角开始拷贝
		$src_w = $this->qrW; //（从 src_x 开始）拷贝的宽度
		$src_h = $this->qrH; //（从 src_y 开始）拷贝的高度

		//合并图片
		imagecopyresized($imgbg, $qrcode, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $src_width, $src_height);
		imagedestroy($qrcode);
		
		header('content-type: image/' . $this->qrType);
		switch($this->qrType){
			case 'jpg':
				imagejpeg($imgbg, $file_name, 50);
				break;
			default:
				imagepng($imgbg, $file_name);
		}
		
		imagedestroy($imgbg);

		if ($file_name === null) {
			die();
		} else {
			return $file_name;
		}
	}

	/**
	 * 保存为文件
	 */
	public function save($file_name) {
		return $this->output($file_name);
	}

	/**
	 * 检查配置项是否正确
	 */
	private function saveConfig($config, $key, $validate = false) {
		if (isset($config[$key])) {

			$val = $config[$key];
			if (is_array($validate)) {
				if (!in_array($val, $validate)) {
					throw new Exception("配置项[{$key}]的值({$val})错误，有效值：[" . join(',' . $validate) . "]", 1);
					die();
				}
			} elseif (is_string($validate)) {
				if (!preg_match($validate, $val)) {
					throw new Exception("配置项[{$key}]的值({$val})错误", 1);
					die();
				}
			} else {
				//无需校验
			}

			//设置值
			$conf_item = 'qr' . ucfirst($key);
			$this->$conf_item = $config[$key];

		}
	}

	/**
	 * 生成二维码链接
	 */
	private function getUrl() {
		$url = $this->qrTemplate;

		$url = str_replace('[KEY]', $this->key, $url);

		//替换访问的域名
		$scheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : ($_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
		$root_uri = $scheme . '://' . $_SERVER['SERVER_NAME'] . '/';
		$url = str_replace('[WEB_ROOT]', $root_uri, $url);

		//替换访问的路径
		$ins_arr = explode('/', $_SERVER['SCRIPT_NAME']);
		$ins_arr[count($ins_arr) - 1] = '';
		unset($ins_arr[0]);
		$ins_path = join($ins_arr, '/');
		$url = str_replace('[WEB_PATH]', $ins_path, $url);

		//替换访问的域名和路径
		$url = str_replace('[WEB_URI]', $root_uri . $ins_path, $url);

		return $url;
	}

	/**
	 * 生成二维码
	 */
	private function createQrcode() {
		if (!is_dir($this->cachePath)) {
			mkdir($this->cachePath);
		}

		//二维码落地链接
		$qr_url = $this->getUrl();

		//生成的二维码保存路径
		$qr_img = $this->cachePath . md5($qr_url) . '.png';
		QRcode::png($qr_url, $qr_img, $this->qrLevel, $this->qrMatrix, 2);
		return $qr_img;
	}

	/**
	 * 打开背景图
	 */
	private function openBackground() {

		list($image_path, $image_ext) = $this->getBackground();

		switch ($image_ext) {
		case 'jpg':
			$background = imagecreatefromjpeg($image_path);
			break;
		case 'png':
			$background = imagecreatefrompng($image_path);
			break;
		case 'gif':
			$background = imagecreatefromgif($image_path);
			break;
		default:
			throw new Exception("不支持{$image_ext}格式的背景文件", 1);
			die();
		}
		return $background;
	}

	/**
	 * 获取背景图信息
	 */
	private function getBackground() {

		//获取图片后缀
		$image_arr = explode('.', $this->qrBackground);
		$image_ext = $image_arr[count($image_arr) - 1];

		//图片的路径
		$image_path = str_replace('[SKIN]', './qrcode.' . $this->qrSkin . '/', $this->qrBackground);

		return [$image_path, $image_ext];
	}

}

?>